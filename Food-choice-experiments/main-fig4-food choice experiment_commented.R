# This R code pertains to the preparation and visualization of results from a food choice experiment, focusing on different leaf treatments. The scripts load the necessary libraries, set various parameters and color schemes, import data from an Excel file, preprocess the data, conduct statistical analyses including ANOVA and Tukey's HSD, and finally generate plots to visualize the findings. The plots are saved in a designated directory.

# Load required libraries
library(XLConnect)
library(doBy)
library(tidyverse)
library(patchwork)
library(ggpubr)
library(scales)
library(ggbeeswarm)
library(rstatix)

# Set color palette and theme for the plots
palette("okabe-ito")
palette()
theme_set(theme_pubr())

# Define custom color mapping for different treatments
cvd_cols <- c("NP" = palette()[2], "FS" =  palette()[3]
              "NP + FS" = palette()[4], "Control" = palette()[9],
              "Sterile" = palette()[8], "Alder" = palette()[7])

# Define directories for figures and data
fig_folder <- "~/Dokumente/Daten/MARS/Manuscripts/leaf degradation/manuscript/figures/2023"
data_folder <- "~/Dokumente/Daten/MARS/Manuscripts/leaf degradation/"
setwd(data_folder)

# Load the Excel workbook and list its sheets
wb <- loadWorkbook("Food choice.xlsx")
getSheets(wb)

# Panel b - Analysis for paired leaf discs
# Import data from the "double treatment" sheet
data <- readWorksheet(wb, "double treatment")

# Clean and transform data
data <- data[-which(data$data.treatment == "rm"),]
data <- data %>% 
  mutate(new.levels = as.character(treatment)) %>%
  # Map old levels to new levels
  mutate(new.levels = ifelse(new.levels == "Nutrients", "NP", new.levels)) %>%
  mutate(new.levels = ifelse(new.levels == "Fine sediment", "FS", new.levels)) %>%
  mutate(new.levels = ifelse(new.levels == "Nutr. + Fine s.", "NP + FS", new.levels)) %>%
  mutate(new.levels = ifelse(new.levels == "No pre-treatment", "Control", new.levels)) %>%
  mutate(treatment = as.factor(new.levels))

# Perform ANOVA on the transformed data
aov.2 <- aov(difference ~ treatment, data = data)
summary(aov.2)

# Post-hoc analysis using Tukey's HSD
t <- tukey_hsd(aov.2)
print(data.frame(t$group1,t$group2, t$p.adj.signif))

# Diagnostic plots for ANOVA assumptions
plot(residuals(aov.2) ~ fitted(aov.2))
qqnorm(residuals(aov.2)); qqline(residuals(aov.2))

# Prepare data to annotate the plot with group information
group <- cbind(levels(data$treatment), c("A", "BC", "C", "AB", "BC"))
h.data <- data
hinge <- h.data %>% group_by(treatment) %>% summarize(q75 = quantile(difference, probs = 0.75))
grps <- tibble(x = 1:5 - 0.2, y = hinge$q75+0.04, labels = group[,2], new.levels = group[,1])

# Create the plot for paired leaf discs data
b <- ggplot(data = data, aes(x = new.levels, y = difference, fill = new.levels, color = new.levels)) + 
  geom_boxplot(alpha = 0.5) +
  geom_hline(yintercept = 0, lty = 1, color = cvd_cols[which(names(cvd_cols) == "Sterile")]) +
  geom_beeswarm() +
  geom_text(data = grps, aes(x = x, y = y, label = labels)) +
  ylab(expression(paste("Dry mass loss (mg"[leaf], " ","mg"[animal]^-1, ")"))) +
  xlab("Leaf species or pre-conditioning") +
  theme(legend.position = "none") +
  scale_fill_manual(values = cvd_cols) +
  scale_color_manual(values = cvd_cols)

# Panel a - Analysis for single leaf discs
# Import data from the "Single treatment" sheet
data.single <- readWorksheet(wb, "Single treatment")

# Remove extreme outliers for specific treatments
in_z <- data.single$Bladnedbrydning.GP[which(data.single$treatment == "FS")]
hist(in_z, breaks = 20)
z <- (in_z - mean(in_z)) / sd(in_z)
in_z[which(z > 2)]
in_z <- data.single$Bladnedbrydning.GP[which(data.single$treatment == "N")]
hist(in_z, breaks = 20)
z <- (in_z - mean(in_z)) / sd(in_z)
in_z[which(z > 2)]
data.single_rm <- data.single %>% dplyr::filter(!(treatment == "FS" & Bladnedbrydning.GP > 0.63)) %>% dplyr::filter(!(treatment == "N" & Bladnedbrydning.GP > 0.90))

# Transform single leaf discs data
data.single2 <- data.single_rm %>% 
  mutate(new.levels = as.character(treatment)) %>%
  # Map old levels to new levels
  mutate(new.levels = ifelse(new.levels == "K", "Sterile", new.levels)) %>%
  mutate(new.levels = ifelse(new.levels == "N", "NP", new.levels)) %>%
  mutate(new.levels = ifelse(new.levels == "NFS", "NP + FS", new.levels)) %>%
  mutate(new.levels = ifelse(new.levels == "No", "Control", new.levels)) %>%
  mutate(treatment = as.factor(new.levels)) %>% 
  mutate(treat.f = factor(treatment, levels(treatment)[c(5,1:4)]))

data.single <- data.single2

# Statistical analysis and diagnostics for single leaf discs data
aov.single <- aov(Bladnedbrydning.GP ~ treat.f, data = data.single)
summary(aov.single)
t <- tukey_hsd(aov.single)
print(data.frame(t$group1,t$group2, t$p.adj.signif))
plot(residuals(aov.single) ~ fitted(aov.single))
qqnorm(residuals(aov.single)); qqline(residuals(aov.single))

# Prepare data to annotate the plot with group information for single discs data
group <- cbind(levels(data.single$treat.f), c("B", "A", "B", "AB", "B"))
hinge <- data.single %>% group_by(treat.f) %>% summarize(q75 = quantile(Bladnedbrydning.GP, probs = 0.75))
grps <- tibble(x = 1:5 - 0.2, y = hinge$q75+0.02, labels = group[,2], new.levels = group[,1])

# Create the plot for single leaf discs data
a <- ggplot(data = data.single, aes(x = treat.f, y = Bladnedbrydning.GP,
              fill = new.levels, color = new.levels)) + 
  geom_boxplot(alpha = 0.5) +
  geom_beeswarm() +
  geom_text(data = grps, aes(x = x, y = y, label = labels)) +
  ylab(expression(paste("Dry mass loss (mg"[leaf], " ","mg"[animal]^-1, ")"))) +
  xlab("Pre-conditioning") + 
  theme(legend.position = "none") +
  scale_fill_manual(values = cvd_cols) +
  scale_color_manual(values = cvd_cols)


# Combine plots for Panel A and Panel B
all <- a + b + plot_layout(ncol = 1) + plot_annotation(tag_level = "A")

# Set directory for saving the figure
setwd(fig_folder)

# Save the combined figure to a file
ggsave("main-fig4-food choice experiment.png", all, width = 4, height = 8, dpi = 300)
