# Content

This repository contains the R code scripts and data for the manuscript titled "Multiple stressor effects act primarily on microbial leaf decomposers in stream mesocosms" by Rasmussen et al.

# R scripts

All data and R scripts for the analysis are contained within the respective folders for the [main text](#main-text) and the [supplementary information](#supplementary-information). All R scripts are commented in great detail to allow for understanding of the analyses, and repetition and display of the plots. **If you wish to repeat the analysis please check the following:**

- R version must be R version 4.3 or newer (see also section [R version and packages](#r-version-and-packages)).
- Packages must be downloaded and available, including their dependencies (see also section [R version and packages](#r-version-and-packages)).
- The source directories and export directories within the R scripts must be adapted to the user needs, these are currently set to the author's preferences.

## Main text 

1. [Principal response curve figure and statistics](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Principal-response-curves) as used in Figure 1 of the main text.

2. [Leaf decomposition rate figure and statistics](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Leaf-decomposition-rate)  as used in Figure 2 of the main text.

3. [Combined ergosterol, leaf degradation and PRC plot](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Combined-plot-ergost-leaf_degrad_PRC) shown in Figure 3 of the main text.

4. [Food choice experiment results](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Food-choice-experiments) shown in Figure 4 of the main text.

## Supplementary information

1. [Ergosterol plot](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Ergosterol-plot) shown in Figure S4.

2. [Differential leaf degradation plot](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Differential-leaf-decomposition) of differences between microbial (fine-mesh bags) and macroinvertebrate consumer + microbial (coarse mesh bags) decomposition rates shown in Figure S5.

3. [Regressions between Gammarus pulex density and leaf decomposition rate](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/leaf_decomp-G_pulex) shown in Figure S6.

# Data

**All data** for the R scripts is in the [data](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/tree/master/Data) subfolder.


1. Food_choice.xslx contains all data of the food choice experiments
2. Leaf_Master.xlsx contains hyphomycete spore composition data and ergosterol concentration data from the flume experiment
3. MARS_leaf_decomposition_all_data.xlsx contains all leaf degradation rates from the flume experiment
4. Shredder_density_per_sample.csv contains the shredder densities in each flume at each sampling time. It was calculated based on macroinvertebrate data published here: Graeber, D., Jensen, T.M., Rasmussen, J.J., Riis, T., Wiberg-Larsen, P., Baattrup-Pedersen, A., 2017. Multiple stress response of lowland stream benthic macroinvertebrates depends on habitat type. Science of The Total Environment 599–600, 1517–1523. https://doi.org/10.1016/j.scitotenv.2017.05.102


# R version and packages

R version 4.3 was used in the data analyses.

The R code within this repository only uses the following R libraries, all available in [CRAN](https://cran.r-project.org):

1. [tidyverse](https://cran.r-project.org/web/packages/tidyverse/)
2. [ggpubr](https://cran.r-project.org/web/packages/ggpubr/)
3. [patchwork](https://cran.r-project.org/web/packages/patchwor/)
4. [scales](https://cran.r-project.org/web/packages/scales/)
5. [doBy](https://www.rdocumentation.org/packages/doBy/versions/4.6.16)
6. [XLConnect](https://cran.r-project.org/web/packages/XLConnect/index.html)
7. [ggrepel](https://cran.r-project.org/web/packages/ggrepel/index.html)
8. [ggbeeswarm](https://cran.r-project.org/web/packages/ggbeeswarm/index.html)


# Disclaimers

The authors do not take any responsibility for damage done to the user system by executing the code of this repository.

The code is distributed under the [BSD-3 license](https://git.ufz.de/graeber/leaf-litter-degradation-mesocosm-microcosm/-/blob/master/LICENSE).

The data is distributed under the license given by the journal and/or publisher of the journal the data is published in.
