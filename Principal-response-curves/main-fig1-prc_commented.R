# This R script performs a Principal Response Curve (PRC) analysis on hyphomycete data for Figure 1 of the study.

# Key steps include:
# 1. Loading necessary libraries and setting the working directory.
# 2. Data preprocessing, including normalizing data by volume and removing zero rows.
# 3. PRC analysis for the 'normal-flow' phase, extracting species and site scores, and preparing data for plotting.
# 4. Setting up plot aesthetics, including color palettes, shapes, and line types.
# 5. Plotting PRC scores for the 'normal-flow' phase and creating a rug plot to display species scores.
# 6. PRC analysis for the 'low-flow' phase and similar data preparation for plotting.
# 7. Plotting PRC scores for the 'low-flow' phase and creating another rug plot for species scores.
# 8. Combining the plots for both phases and saving the final combined plot to a file.


# Load required libraries for the analysis
library(XLConnect)      # XLConnect: Allows for reading, writing, and manipulating Excel files
library(vegan)          # vegan: Provides functions for community ecology analysis
library(tidyverse)      # tidyverse: Collection of packages for data manipulation and visualization
library(ggpubr)         # ggpubr: Provides functions to arrange ggplot2 plots with tables and texts
library(ggrepel)        # ggrepel: Avoid text label overlap in ggplot2 plots
library(patchwork)      # patchwork: Combine and arrange multiple ggplots
library(scales)         # scales: Functions for scaling and formatting axis labels and legends in ggplot2


# Define directories for figures and data
fig_folder <- "~/Dokumente/Daten/MARS/Manuscripts/leaf degradation/manuscript/figures/2023"
data_folder <- "~/Dokumente/Daten/MARS/Manuscripts/leaf degradation/"

# Set the working directory to the specified data folder
setwd(data_folder)

# Load the Excel workbook containing the data
wb <- loadWorkbook("Leaf Master.xlsx")

# List the sheets available in the workbook
getSheets(wb)

# Read the data from the "species.matrix.final.names" sheet
data <- readWorksheet(wb, "species.matrix.final.names", check.names = FALSE)

# Calculate the total count of each species across all samples
colSums(data[,10:ncol(data)])

# Convert species count to spores per mL by dividing by volume and multiplying by 1000
data[,10:ncol(data)] <- (data[,10:ncol(data)] / data$vol) * 1000

# Identify and remove samples where all species counts are zero
rm.vec <- which(rowSums(data[,10:ncol(data)]) == 0)  # Identify empty samples
data[rm.vec, 1:9]  # Display metadata of the empty samples

# Create a new dataset 'beech' without the empty samples
beech <- data[-rm.vec,]

# Convert 'treatment' column to a factor with specific levels
beech$treatment <- factor(beech$treatment, levels = c("no.sec", "fs", "np", "np+fs"))

# Subset the data for the 'normal-flow' phase
phase <- beech[beech$phase == "nf",]

# Select species data from the 'phase' dataset
sp <- phase[,10:ncol(phase)]

test <- capscale(log(sp+1) ~ treatment, dist = "bray", data = phase)
summary(test)
adonis2(log(sp+1) ~ treatment, dist = "bray", data = phase)
plot(test)

test <- capscale(log(sp+1) ~ as.factor(fs) + as.factor(np), dist = "bray", data = phase)
plot(test)
adonis2(log(sp+1) ~ as.factor(fs) * as.factor(np), dist = "bray", data = phase)
plot(test)



# Convert species count data to presence-absence (1 if present, 0 if absent)
sp[which(sp > 0, arr.ind = T)] <- 1

# Perform PRC analysis using capscale with Jaccard distance
# treatment and week are predictor variables, with week treated as a factor
prc.data <- capscale(sp ~ treatment * as.factor(wk) + Condition(as.factor(wk)), 
                     data = phase, distance = "jaccard", sqrt.dist= T)

# Display summary of the PRC analysis
summary(prc.data, display = NA)

# Perform ANOVA on the PRC results
anova(prc.data, first = TRUE)

# Extract species scores from the PRC analysis
scr.sp <- scores(prc.data, scaling = 3)$species[,1]
rownames(scr.sp)  # Display the species names

# Extract site scores from the PRC analysis
scr.st <- scores(prc.data, scaling = 3)$sites

# Combine treatment, channel, week, and PRC site scores into a data frame
scr.1 <- data.frame(phase$treatment, phase$channel, phase$wk, scr.st[,1])
names(scr.1) <- c("prc.treat", "channel", "week", "prc1")

# Convert 'week' in scr.1 to a factor
zeit <- as.factor(scr.1$week)

# Initialize an empty object to store results
all <- NULL

# Loop through each level of 'zeit' (week)
for (i in 1:length(levels(zeit))) {
    # Subset data for the current week
    treat <- scr.1[as.character(zeit) == levels(zeit)[i],]
    
    # Calculate the relative change in PRC scores by subtracting the mean of 'no.sec' group from others
    rel <- treat[treat$prc.treat != "no.sec", 4] - mean(treat[treat$prc.treat == "no.sec", 4])
    
    # Combine the relative change with treatment, channel, and week data
    rel <- cbind(treat[treat$prc.treat != "no.sec", 1:3], rel)
    
    # Append the results to 'all'
    all <- rbind(all, rel)
}

# Drop any unused levels in the 'all' dataset
all <- droplevels(all)

# Determine a threshold for species scores to include in the plot
min.scr.sp <- 0.2

# Subset species with scores above the threshold
short.plot <- scr.sp[abs(scr.sp) > min.scr.sp]

# Create a data frame with species names and their scores for plotting
sp.plot <- short.plot
sp.df <- data.frame(Species = names(sp.plot), sp.plot)

# Set the color palette to "okabe-ito" for the plots
palette("okabe-ito")

# Display the colors in the chosen palette
palette()
show_col(palette())

# Set the plot theme to 'theme_pubr' from the ggpubr package
theme_set(theme_pubr())

# Define shapes for each treatment type
treat_shapes <- c('np' = 18, 'fs' =  15, 'np+fs' = 17, 'c' = NA)

# Define colors for each treatment type based on the chosen palette
cvd_cols <- c('np' = palette()[2], 'fs' =  palette()[3], 'np+fs' = palette()[4], 'c' = palette()[9])

# Define line types for each treatment type
cvd_lty <- c('np' = 1, 'fs' =  1, 'np+fs' = 1, 'c' = 1)

# Specify full names for each treatment type for the plot legend
plot_names <- c('np' = "Nutrient enrichment (NP)", 'fs' = "Fine sediment (FS)", 'np+fs' = "NP + FS", 'c' = "Control")

# Display the colors associated with each treatment type
show_col(cvd_cols)

# Summarize the 'all' dataset to get mean and standard deviation of 'rel' grouped by treatment and week
prc.summ <- all %>% group_by(prc.treat, week) %>%
            summarise(mean_rel = mean(rel), sd_rel = sd(rel))

# Add a control group with mean and sd values of 0 for plotting
prc.summ2 <- rbind(prc.summ, data.frame(prc.treat = rep("c",3), week = c(1,2,4), mean_rel = rep(0,3), sd_rel = rep(0,3)))

# Keep table for SI summary PRC table
prc.summ.nf <- prc.summ2

# Start plotting PRC scores using ggplot
p_prc_scores <- ggplot() +
    geom_point(prc.summ2, mapping = aes(y = mean_rel, x = week, color = prc.treat, shape = prc.treat), alpha = 0.8, size = 3) +
    geom_line(prc.summ2, mapping = aes(y = mean_rel, x = week, color = prc.treat, linetype = prc.treat), alpha = 0.8) +
# Set y-axis limits, labels, and other plot aesthetics
ylim(c(-2, 1.5)) +
ylab("PRC score") +
xlab(element_blank()) +
theme(panel.spacing.y = unit(0, "cm"), legend.position = c(0.35, 0.85)) +
scale_x_continuous(breaks = c(1,2,4)) +
scale_colour_manual(values = cvd_cols, name = "Treatment", labels = plot_names) +
scale_shape_manual(values = treat_shapes, name = "Treatment", labels = plot_names) +
scale_linetype_manual(values = cvd_lty, name = "Treatment", labels = plot_names)

# Initialize a ggplot object for plotting species scores as a rug plot
p_rug <- ggplot(sp.df, mapping = aes(x = 1, y = sp.plot, label = Species)) + 
      geom_rug(color = "black", sides = "l") +  # Create a rug plot on the left side
      xlim(0, 2) +
      ylim(c(-2, 1.5)) +
theme( # Remove all plot elements to retain only the rug plot
    axis.line  = element_blank(),
    axis.ticks = element_blank(),
    axis.text  = element_blank(),
    axis.title = element_blank(),
    panel.border = element_blank(), # Add additional theme settings to the rug plot
    plot.margin = margin(0, 0, 0, 0, "cm"),
    panel.spacing = unit(0, "cm")
) 

# Modify the x-axis limits and add species names using geom_text_repel
p_sp <- p_rug +
      xlim(1, 1.75) + 
      geom_text_repel(      
      force = 1.5,           # Force of repulsion between text labels
      nudge_x = 0.15,        # Horizontal adjustment of text labels
      direction = "y",       # Direction of repulsion
      hjust = 0,             # Horizontal justification of text
      segment.size = 0.2,    # Size of the segment connecting the text to the rug
      size = 4               # Size of the text label
      ) +
      theme_pubr() +        # Apply the 'theme_pubr' for the plot
theme(
    axis.line  = element_blank(), # Remove all plot elements to retain only the rug plot with species names
    axis.ticks = element_blank(),
    axis.text  = element_blank(),
    axis.title = element_blank(),
    panel.border = element_blank(), 
    plot.margin = margin(0, 0, 0, 0, "cm"), # Add additional theme settings to the rug plot
    panel.spacing = unit(0, "cm")
) 

# Combine the PRC scores plot (p_prc_scores) with the species scores rug plot (p_sp) side by side
p_norm <- p_prc_scores + p_sp + plot_layout(widths = c(1,1))  + plot_annotation(tag_levels = list("A"))

# Display the combined plot
p_norm



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# PRC Analysis for Low-Flow Phase

# Subset the data for the 'low-flow' phase
phase <- beech[beech$phase == "lf",]

# Select species data from the 'phase' dataset
sp <- phase[,10:ncol(phase)]

# Convert species count data to presence-absence (1 if present, 0 if absent)
sp[which(sp > 0, arr.ind = T)] <- 1

# Perform PRC analysis using capscale with Jaccard distance for the low-flow phase
# treatment and week are predictor variables, with week treated as a factor
prc.data <- capscale(sp ~ treatment * as.factor(wk) + Condition(as.factor(wk)), 
                     data = phase, distance = "jaccard", sqrt.dist= T)

# Display summary of the PRC analysis
summary(prc.data, display = NA)

# Perform ANOVA on the PRC results for the low-flow phase
anova(prc.data, first = TRUE)

# Extract species scores from the PRC analysis
scr.sp <- scores(prc.data, scaling = 3)$species[,1]
rownames(scr.sp)  # Display the species names

# Extract site scores from the PRC analysis
scr.st <- scores(prc.data, scaling = 3)$sites

# Combine treatment, channel, week, and PRC site scores into a data frame
scr.1 <- data.frame(phase$treatment, phase$channel, phase$wk, scr.st[,1])
names(scr.1) <- c("prc.treat", "channel", "week", "prc1")

# Convert 'week' in scr.1 to a factor
zeit <- as.factor(scr.1$week)

# Initialize an empty object to store results
all <- NULL

# Loop through each level of 'zeit' (week) to calculate relative change similar to previous section
for (i in 1:length(levels(zeit))) {
    treat <- scr.1[as.character(zeit) == levels(zeit)[i],]
    rel <- treat[treat$prc.treat != "no.sec", 4] - mean(treat[treat$prc.treat == "no.sec", 4])

# Continue calculating relative change and appending results to 'all'
rel <- cbind(treat[treat$prc.treat != "no.sec", 1:3], rel)
all <- rbind(all, rel)
}

# Drop any unused levels in the 'all' dataset
all <- droplevels(all)

# Invert the PRC scores for plotting
all[,4] <- all[,4]*-1
scr.sp <- scr.sp*-1

# Determine a threshold for species scores to include in the plot
min.scr.sp <- 0.2

# Subset species with scores above the threshold
short.plot <- scr.sp[abs(scr.sp) > min.scr.sp]

# Create a data frame with species names and their scores for plotting
sp.plot <- short.plot
sp.df <- data.frame(Species = names(sp.plot), sp.plot)

# Summarize the 'all' dataset to get mean and standard deviation of 'rel' grouped by treatment and week
prc.summ <- all %>% group_by(prc.treat, week) %>%
            summarise(mean_rel = mean(rel), sd_rel = sd(rel))

# Add a control group with mean and sd values of 0 for plotting
prc.summ2 <- rbind(prc.summ, data.frame(prc.treat = rep("c",3), week = c(1,2,4), mean_rel = rep(0,3), sd_rel = rep(0,3)))

# Keep table for SI summary PRC table
prc.summ.lf <- prc.summ2


# Start plotting PRC scores using ggplot
p_prc_scores <- ggplot() +
    geom_point(prc.summ2, mapping = aes(y = mean_rel, x = week, color = prc.treat, shape = prc.treat), alpha = 0.8, size = 3) +
    geom_line(prc.summ2, mapping = aes(y = mean_rel, x = week, color = prc.treat, linetype = prc.treat), alpha = 0.8) +
    ylim(c(-2, 1.5)) +
    ylab("PRC score") +
    xlab("Week") +
    theme(panel.spacing.y = unit(0, "cm"), legend.position = c(0.35, 0.85)) +
    scale_x_continuous(breaks = c(1,2,4)) +
    scale_colour_manual(values = cvd_cols, name = "Treatment", labels = plot_names) +
    scale_shape_manual(values = treat_shapes, name = "Treatment", labels = plot_names) +
    scale_linetype_manual(values = cvd_lty, name = "Treatment", labels = plot_names)

# Initialize a ggplot object for plotting species scores as a rug plot
p_rug <- ggplot(sp.df, mapping = aes(x = 1, y = sp.plot, label = Species)) + 
      geom_rug(color = "black", sides = "l") +  # Create a rug plot on the left side
      xlim(0, 2) +
      ylim(c(-2, 1.5)) +
# Remove all plot elements to retain only the rug plot
theme(
    axis.line  = element_blank(),
    axis.ticks = element_blank(),
    axis.text  = element_blank(),
    axis.title = element_blank(),
    panel.border = element_blank(),
    panel.spacing.y = unit(0, "cm"),
    plot.margin = margin(0, 0, 0, 0, "cm"),
    panel.spacing = unit(0, "cm")
) 

# Modify the x-axis limits and add species names using geom_text_repel
p_sp <- p_rug +
      xlim(1, 1.75) + 
      geom_text_repel(      
      force = 1.5,           # Force of repulsion between text labels
      nudge_x = 0.15,        # Horizontal adjustment of text labels
      direction = "y",       # Direction of repulsion
      hjust = 0,             # Horizontal justification of text
      segment.size = 0.2,    # Size of the segment connecting the text to the rug
      size = 4               # Size of the text label
      ) +
      theme_pubr() +         # Apply the 'theme_pubr' for the plot
# Continue applying the theme to remove unwanted plot elements for the rug plot
theme(
    axis.line  = element_blank(),
    axis.ticks = element_blank(),
    axis.text  = element_blank(),
    axis.title = element_blank(),
    panel.spacing.y = unit(0, "cm"),
    panel.border = element_blank(),     
    plot.margin = margin(0, 0, 0, 0, "cm"),
    panel.spacing = unit(0, "cm")
)

# Combine the PRC scores plot (p_prc_scores) for the low-flow phase with the species scores rug plot (p_sp) side by side
p_low <- p_prc_scores + p_sp + plot_layout(widths = c(1,1))
p_low

# Combine the plots for the 'normal-flow' phase and the 'low-flow' phase vertically
p_all <- p_norm / p_low + plot_annotation(tag_levels = list(c("A", "", "B", "")))

# Save the combined plot to a file
ggsave(plot = p_all, filename = paste(fig_folder, "main-fig1-prc.png", sep="/"), width = 7, height = 10, dpi = 300)


# Make table for SI
prc.si <- rbind(
            cbind("Phase" = "normal-flow", prc.summ.nf), 
            cbind("Phase" = "low-flow", prc.summ.lf)
)
names(prc.si) <- c("Phase", "Treatment", "Week", "Mean", "SD")

write_csv(prc.si, paste(fig_folder, "fig1-prc-data.csv", sep="/"))
